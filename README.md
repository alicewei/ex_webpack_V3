## 使用工具
 - webpack 3
 - yarn 1.6.0
 - node 8.0.0
 - 自行添加資料夾 public/
 - 使用pug

## 指令
https://localhost:8081

```sh
$ yarn install       // 安裝 node_modules/
$ yarn dev           // 預覽
$ yarn prod          // 產生
$ brew upgrade yarn  // 更新 yarn 版本
```
## 功能
 - uglify JS
 - css link via JS module setting
 - scss -> autoprefixer -> css
 - pug -> html
 - images/ COPY
 - url scan X