const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin'); // html 用
const ExtractTextPlugin = require("extract-text-webpack-plugin"); // css 用
const UglifyJSPlugin = require('uglifyjs-webpack-plugin'); // Js 混淆用
const CopyWebpackPlugin = require('copy-webpack-plugin'); //圖片用
const isProd = process.env.NODE_ENV === 'prod';
const WDSport = '8081';
module.exports = {
    devtool: isProd ? '' : 'source-map',
    devServer: {
        // 設定目錄結構
        contentBase: path.resolve(__dirname, 'public'),
        // server的ip位置
        host: 'localhost',
        // server是否壓縮
        compress: true,
        // server port
        port: WDSport,
        stats: 'minimal'
    },
    // 入口檔案設定
    entry: {
        main: [
            './src/main.js',
            // 這邊還能繼續增加檔案
        ]
    },
    // 出口檔案設定
    output: {
        path: path.resolve(__dirname, 'public'),
        // [name]對應 entry 裡的 key，等於之後打包的檔名，例如: main
        // filename: '[name]-[chunkhash:3].js',
        filename: '[name].js',
        publicPath: ''
    },
    // 模組，loader 內的都在這邊設定，這邊執行都是由下往上、由右至左
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.(scss|css)$/,
                use: ExtractTextPlugin.extract({
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: !isProd,
                                importLoaders: 2,
                                minimize: isProd,
                                url: false // 因為底圖寫相對路徑，掃到相對會報錯，但是上傳到網路空間必須是相對的
                            }
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function() {
                                    return [
                                        require('postcss-css-reset'),
                                        require('autoprefixer')({
                                            browsers: [ '> 5%' ]
                                        })
                                    ];
                                }
                            }
                        },
                        {
                            loader: "sass-loader"
                        }
                    ],
                    fallback: 'style-loader',
                    publicPath: './public'
                })
            },
            {
                test: /\.(jpg|png|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]',
                    context: '',
                    emitFile: false,
                    useRelativePath: true
                }
                //確保不掃 html 內 img 圖片來源，但還是會掃連結到圖片的 url
            },
            {
                test: /\.pug$/,
                loader: ['html-loader', 'pug-html-loader'] //確保不掃 src 連結內的 url 嗎？
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.pug']
    },
    // 外掛
    plugins: [
        new UglifyJSPlugin(),
        new CopyWebpackPlugin([
            {
                from: 'src/images',
                to: 'images/'
            }
        ]),
        // html 壓縮外掛
        new HtmlWebpackPlugin({
            template: './src/index.pug',
            filename: './index.html',
            hash: true
        }),
        new ExtractTextPlugin({
            filename: "main.css",
            disable: false,
            // allChunks: true
        })
    ]
};